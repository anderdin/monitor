<?php

/**
 * Description of ControleGruposUsuarios
 * Classe de espelho para tabela controle_grupos_usuarios do banco de dados
 * @author Felipe Silva
 */

include_once '../helpers/Conexao.php';

class ControleGruposUsuarios {
    public $idcontrole_grupos_usuarios = "";
    public $controle_grupos_usuarios_id_usuario = "";
    public $controle_grupo_usuarios_id_grupo = "";
    
    function buscarControleGrupoUsuariosId($ControleGruposUsuarios){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("select * from controle_grupos_usuarios where controle_grupos_usuarios.idcontrole_grupos_usuario = '{$ControleGruposUsuarios->idcontrole_grupos_usuario}'")->fetchAll();
        
        return $res;
    }
    
    
}
