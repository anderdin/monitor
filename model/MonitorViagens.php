<?php
/**
 * Description of Usuarios
 * Classe de espelho para a tabela Monitor Viagens do banco de dados
 * @author Felipe
 */

include_once '../helpers/Conexao.php';

class MonitorViagens {
    public $idoperacaok1 = "";

    
    function buscarTodos(){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("select * from monitor_viagens_integracao")->fetchAll();
        
        return $res;
    }
    

}
