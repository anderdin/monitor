<?php
/**
 * Description of usuarios
 * Classe de espelho para a tabela usuarios do banco de dados
 * @author Felipe Silva
 */

include_once '../helpers/Conexao.php';

class Usuarios {
    public $idusuarios = "";
    public $usuarios_login = "";
    public $usuarios_senha = "";
    public $permissao = "";
    public $usuarios_email = "";
    public $grupos = "";

    function buscarUsuario($usuario){
       $conexao = new Conexao();
       $conn = $conexao->local();
       $sql = "select * from usuarios where usuarios_login = '{$usuario->usuarios_login}' and usuarios_senha = md5('{$usuario->usuarios_senha}')";
   
       $res = $conn->query($sql)->fetchAll();
       
       return $res;
    }

     function confiUsuario($usuario){
       $conexao = new Conexao();
       
       $conn = $conexao->local();

       $sql = "SELECT * FROM usuarios WHERE idusuarios = '{$usuario->idusuarios}'";
   
       $res = $conn->query($sql);
       
       return $res;
    }
    
    /*Elaborar outro nome para está função*/
    function confiGruposUsuarios($usuario){
        $conexao = new Conexao();
       
        $conn = $conexao->local();

        $sql = "SELECT  D.grupos_nome, D.idgrupos FROM usuarios as A
               INNER JOIN controle_grupos_usuarios as B ON A.idusuarios = B.controle_grupos_usuarios_id_usuario
               INNER JOIN controle_monitores_grupo as C ON C.controle_monitores_grupo_idGrupo = B.controle_grupos_usuarios_id_grupo
               INNER JOIN grupos as D ON B.controle_grupos_usuarios_id_grupo = D.idgrupos 
               INNER JOIN monitores as E ON E.idmonitores = C.idcontrole_monitores_grupo
               WHERE A.idusuarios = '{$usuario->idusuarios}' GROUP BY D.grupos_nome";

        $res = $conn->query($sql);
       
        return $res;
    }

    function confiUpdateUsuario($loginConfi){
       $conexao = new Conexao();
       
       $conn = $conexao->local();

       $sql = "UPDATE usuarios SET
                usuarios_login = '{$loginConfi->usuarios_login}',
                usuarios_senha = '{$loginConfi->usuarios_senha}',
                usuarios_email = '{$loginConfi->usuarios_email}',
                usuarios_permissao_nivel = '{$loginConfi->permissao}'

                WHERE idusuarios = {$loginConfi->idusuarios}'";
   
       $res = $conn->query($sql);
       return $res;
    }
    
    function buscarTodos($usuario){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("
                SELECT DISTINCT A.usuarios_login, D.monitores_tecnologia, E.grupos_nome  FROM usuarios A 
                INNER JOIN controle_grupos_usuarios B ON A.idusuarios = B.controle_grupos_usuarios_id_usuario
                INNER JOIN controle_monitores_grupo C ON B.controle_grupos_usuarios_id_grupo = C.controle_monitores_grupo_idGrupo
                INNER JOIN monitores D ON D.idmonitores = C.controle_monitores_grupo_idMonitor
                INNER JOIN grupos E ON E.idgrupos = B.controle_grupos_usuarios_id_grupo WHERE A.idusuarios ='{$usuario->idusuarios}' GROUP BY grupos_nome")->fetchAll();
        
        return $res;
    }
    
    function cadastrarUsuarios($usuariosModel){
        $conexao = new Conexao();

        $conn = $conexao->local();
        
        $sql = "insert into usuarios set "
                . "usuarios_login = '{$usuariosModel->usuarios_login}',"
                . "usuarios_senha = md5('padrao#9999'),"
                . "usuarios_permissao_nivel = '{$usuariosModel->permissao}',"
                . "usuarios_email = '{$usuariosModel->usuarios_email}';";
       
        //$grupos[] = explode(",", $usuariosModel->grupos);
        $res = $conn->query($sql);

        foreach($usuariosModel->grupos as $grupoUsuarios){
        
            $sql1 .= "insert into controle_grupos_usuarios set "
                . "controle_grupos_usuarios_id_usuario = (SELECT MAX(idusuarios) FROM usuarios),"
                . "controle_grupos_usuarios_id_grupo = '{$grupoUsuarios}';";
         
        }

        $res = $conn->query($sql1);
      
        return $res;

    }

    function atualizarUsuario($loginConfi){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "update usuarios set usuarios_senha = '{$loginConfi->usuarios_senha}' where idusuarios = {$loginConfi->idusuarios}";

        $res = $conn->query($sql);
        return $res;
    }

    function update($update){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "UPDATE usuarios SET 
        usuarios_login = '{$update->usuarios_login}',
        usuarios_senha = '{$update->usuarios_senha}',
        usuarios_email = '{$update->usuarios_email}',
        usuarios_nivel = '{$update->permissao}',
        WHERE idusuarios = {$update->idusuarios}";

        $res = $conn->query($sql);
        return $res;
    }

    
    function deletarusuarios($idusuarios){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $conn->query("delete from usuarios where idusuarios = '{$idusuarios}'");
        
        return true;
    }
}


