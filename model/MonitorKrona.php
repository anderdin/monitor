<?php
/**
 * Description of Usuarios
 * Classe de espelho para a tabela MonitorKrona do banco de dados
 * @author Felipe Silva
 */

include_once '../helpers/Conexao.php';

class MonitorKrona1 {
    
    function buscarTodos(){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("select * from monitor")->fetchAll();
        
        return $res;
    }
    

}
