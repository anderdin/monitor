<?php

/**
 * Description of Monitor
 * Classe de espelho para a tabela monitor no banco de dados
 * @author Anderson
 */

include_once '../helpers/Conexao.php';

class Monitor {
    public $idmonitores = "";
    /*public $monitores_tecnologia = "";
    public $monitores_ultimo_evento_entrada = "";
    public $monitores_ultimo_evento_saida = "";
    public $monitores_transmitir = "";
    public $monitores_referenciar = "";
    public $monitores_qtda_veiculos = "";
    public $monitores_alerta_conexao_internet = "";
    public $monitores_alerta_conexao_entrada = "";
    public $monitores_alerta_conexao_saida = "";
    public $monitores_alerta_atrazo_entrada = "";
    public $monitores_alerta_atrazo_saida = "";
    public $monitores_alerta_atrazo_internet = "";*/
    public $grupos = "";
    
    function buscarMonitoresId($monitores){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("select * from monitores where monitores.idmonitores = '{$monitores}'")->fetchAll();
        
        return $res;
    }
    
    function buscarMonitoresUsuario(){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "select * from monitores as A
                 inner join controle_monitores_grupo as B on B.controle_monitores_grupo_idMonitor = A.idmonitores
                 inner join controle_grupos_usuarios as C on C.controle_grupos_usuarios_id_grupo = B.controle_monitores_grupo_idGrupo
                 where C.controle_grupos_usuarios_id_usuario =".$_SESSION['id_usuario'];
        $res = $conn->query($sql);
        
        return $res;
    }
    
    function buscarTodos(){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $res = $conn->query("select * from monitores")->fetchAll();
        
        return $res;
    }
    
    function buscarMonitoresIdGrupo($idGrupo){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "SELECT  * FROM monitores
                INNER JOIN controle_monitores_grupo ON controle_monitores_grupo.controle_monitores_grupo_idMonitor = monitores.idmonitores
                WHERE controle_monitores_grupo.controle_monitores_grupo_idGrupo = '{$idGrupo}' ";

        $res = $conn->query($sql)->fetchAll();
        return $res;          
    }

    function associacaoMonitor($monitorModel){
        $conexao = new Conexao();
        $conn = $conexao->local();

        $sql = '';
       
         foreach($monitorModel->idmonitores as $monitores){
            foreach($monitorModel->grupos as $grupos){
              
                $sql .= "insert into controle_monitores_grupo set "
                    . "controle_monitores_grupo_idMonitor = '{$monitores}',"
                    . "controle_monitores_grupo_idGrupo = '{$grupos}';";
            }
        }

        $res = $conn->query($sql);
        
        return $res;
    }
}
