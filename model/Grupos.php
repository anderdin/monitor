<?php
/**
 * Description of Grupos
 * Espelho da tabela Grupos do banco de dados.
 * @author Felipe Silva
 */

include_once '../helpers/Conexao.php';

class Grupos {
    public $idgrupos = "";
    public $grupos_nome = "";
    //public $cadastrarGrupo = "";
    
    function buscarGruposId($idGrupo){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "select * from grupos where grupos.idgrupos = '{$idGrupo}'";
        $res = $conn->query($sql)->fetchAll();
        
        return $res;
    }
    
    function buscarCtlGrupoUsuariosIdUsuario($idUsuario){
        $conexao = new Conexao();
        $conn = $conexao->local();
        
        $sql = "select 
                grupos.*
                from grupos
                inner join controle_grupos_usuarios on controle_grupos_usuarios.controle_grupos_usuarios_id_grupo = grupos.idgrupos
                where
                controle_grupos_usuarios.controle_grupos_usuarios_id_usuario = '{$idUsuario}'";
        $res = $conn->query($sql)->fetchAll();
        
        return $res;
    }
    
    function buscarTodos(){
        $conexao = new Conexao();
        
        $conn = $conexao->local();
        
        $sql = "SELECT * FROM grupos GROUP BY grupos_nome";

        $res = $conn->query($sql)->fetchAll();
        
        return $res;
    }

    function cadastrarGrupo($grupoModel){
        $conexao = new Conexao();
        $conn = $conexao->local();

        $sql = "insert into grupos set grupos_nome = '{$grupoModel->grupos_nome}'";

        $res = $conn->query($sql);

        return $res;
    }
}
