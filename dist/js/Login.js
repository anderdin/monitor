function login(){
      var login = $("#login").val();
      var senha = $("#senha").val();

       if(login != '' && senha != ''){
            
           $(this).attr("disabled", "true");
           $("#login").attr("disabled", "true");
           $("#senha").attr("disabled", "true");
           $(this).html("<img src='../dist/imagens/loading.gif' height='16px' width='16px'>");

           $.post("../controller/LoginController.php", {operacao: "logon", login:login, senha:senha }, function(a){
                if(senha=='padrao#9999'){
                  window.location.replace("../views/confirmarSenha.php");
                  alert('Por favor, cadastre uma nova senha.');
                }else{
                   if(a.length > 0){
                       window.location.replace("../views/Monitor.php");
                   }else{
                       $("#erro").removeClass("hidden");
                       $("#erro").addClass("show");
                       $("#login").removeAttr("disabled");
                       $("#senha").removeAttr("disabled");
                       $("#btnEntrar").html("ENTRAR");
                       $("#btnEntrar").removeAttr("disabled");
                   }
                }

                if(a.length <=0){
                  //Mensagem de erro caso os campos estejam vazios
                  $('#alertsLoginErro').css('display','block');
                }
           }, "json").fail(function(b,c,d){
              alert('Falha');
           });
           
       }
       
   if(login == '' || senha == ''){
       //Mensagem de erro caso os campos estejam vazios
        $('#alertsLoginWarning').css('display','block');
   }
}


//Nova senha.
function loginSenha(){
    var idUsuario = $('#idUsuario').val();
      var senha = $('#senha').val();
        var senhaConfirmacao = $('#senhaConfirmacao').val();

      if(senha != senhaConfirmacao){
          $('#alertsConfirmarSenha').css('display','block');
        }else{
          $.post("../controller/LoginController.php", {operacao: "novaSenha", senha:senha, idUsuario:idUsuario }, function(a){
             
                 window.location.replace("../views/Monitor.php");
                 alert('Senha alterada com sucesso.');
         }, "json").fail(function(b,c,d){
             alert('Falha no cadastro da senha.');
         });
      }

}



