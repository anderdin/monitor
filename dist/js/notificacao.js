$(document).ready(function(){
	
	//ocultar/mostrar notificação.
    $('#ocultar_notificacao').click(function() {
        $('#notificacao').toggle('slow');
    });

    $('.button_notificacao').click(function(event){
        if($('#eyes_notificacao').hasClass('glyphicon-eye-open')){
        	$('#eyes_notificacao').attr('class','glyphicon glyphicon-eye-close');
        }else{
        	$('#eyes_notificacao').attr('class','glyphicon glyphicon-eye-open');
        }
    });

	$(".fechar").click(function(event){
	    event.preventDefault();
	    fechar(); // chamada a função
	});

})

function fechar(){
    $("#fechar_notificacao").hide();
}