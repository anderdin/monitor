/*

$(document).ready(function(){
    var cod = $("#codUsuario").val();
    gerarMonitores(cod);
});

function sair(){
    $.post("../controller/LoginController.php", {operacao:"logoff"}, function(a){
            window.location.replace("../view/login.php")
        },"text").fail(function(b,c,d){
            alert(d);
        });
}

function gerarMonitores(id){
    $.post("../Controller/MonitorController.php", {operacao:"carregaMenus", idUsuario:id}, function(a){
        
        if(a.length > 0){
            var html = "<li><a onclick='filtro(0)' href='#'>Todos os Grupos</a></li><li role='separator' class='divider'></li>";
            for(var i = 0; a.length > i; i++){
                //html += "<li><a onclick='filtro("+a[i].controle_grupos_usuarios_id_grupo+")' href='#'>"+a[i].descricao_grupo+"</a></li>";
                html += "<li><a role='button' data-toggle='collapse' href='#"+a[i].idgrupos+"' aria-expanded='false' aria-controls='collapseExample'>"+a[i].grupos_nome+"</a></li>"
            }
            $("#grupos").html(html);
            
            $.post("../controller/MonitorController.php", {operacao:"carregaMonitores", usuario:id}, function(a2){
                
                var html = "";
                if(a2.length > 0){
                    for(var i=0; a2.length > i; i++){
                        html += "<div class='collapse' id='"+a2[i].idgrupos+"'>";
                            html += "<div class='well'>";
                                html += "<table class='table table-bordered' style='text-align: center'>";
                                    html += "<tr><td colspan='9'>"+a2[i].grupos_nome+"</td></tr>";
                                    html += "<tr><td colspan='3'>Base de Dados</td><td colspan='3'>Conexões</td><td colspan='3'>Velocidades</td></tr>";
                                    $teste = a2[i].monitores.length;
                                    for(var j = 0; a2[i].monitores.length > j; j++){
                                        
                                        switch (a2[i].monitores[j].monitores_alerta_conexao_entrada){
                                            case "0":
                                                var alEntrada = "Online";
                                                break;
                                            case "1":
                                                var alEntrada = "Ocilando";
                                                break;
                                            case "2":
                                                var alEntrada = "Offline";
                                                break;
                                        }
                                        switch (a2[i].monitores[j].monitores_alerta_conexao_saida){
                                            case "0":
                                                var alSaida = "Online";
                                                break;
                                            case "1":
                                                var alSaida = "Ocilando";
                                                break;
                                            case "2":
                                                var alSaida = "Offline";
                                                break;
                                        }
                                        switch (a2[i].monitores[j].monitores_alerta_conexao_internet){
                                            case "0":
                                                var alInternet = "Online";
                                                break;
                                            case "1":
                                                var alInternet = "Ocilando";
                                                break;
                                            case "2":
                                                var alInternet = "Offline";
                                                break;
                                        }
                                        switch (a2[i].monitores[j].monitores_alerta_atrazo_entrada){
                                            case "0":
                                                var atEntrada = "Rapida";
                                                break;
                                            case "1":
                                                var atEntrada = "Ocilante";
                                                break;
                                            case "2":
                                                var atEntrada = "Lenta";
                                                break;
                                        }
                                        switch (a2[i].monitores[j].monitores_alerta_atrazo_saida){
                                            case "0":
                                                var atSaida = "Rapida";
                                                break;
                                            case "1":
                                                var atSaida = "Ocilante";
                                                break;
                                            case "2":
                                                var atSaida = "Lenta";
                                                break;
                                        }
                                        switch (a2[i].monitores[j].monitores_alerta_atrazo_internet){
                                            case "0":
                                                var atInternet = "Rapida";
                                                break;
                                            case "1":
                                                var atInternet = "Ocilante";
                                                break;
                                            case "2":
                                                var atInternet = "Lenta";
                                                break;
                                        }
                                        
                                        html += "<tr>";
                                            html += "<td>"+a2[i].monitores[j].monitores_tecnologia+"</td>";
                                            html += "<td>"+a2[i].monitores[j].monitores_ultimo_evento_entrada+"</td>";
                                            html += "<td>"+a2[i].monitores[j].monitores_ultimo_evento_saida+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_conexao_entrada+"'>"+alEntrada+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_conexao_saida+"'>"+alSaida+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_conexao_internet+"'>"+alInternet+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_atrazo_entrada+"'>"+atEntrada+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_atrazo_saida+"'>"+atSaida+"</td>";
                                            html += "<td class='status"+a2[i].monitores[j].monitores_alerta_atrazo_internet+"'>"+atInternet+"</td>";
                                        html += "</tr>";
                                    }
                                html += "</table>";
                            html += "</div>";
                        html += "</div>";
                    }
                }else{
                    html += "<h1>Nenhum monitor atrelado a este usuario</h1>"; 
                }
                $("#painel").html(html);
            }, "json").fail(function(b2,c2,d2){
                alert(d2);
            });
            
        }else{
            
        }
    }, "json").fail(function(b,c,d){
        alert(d);
    });
}

function adm(){
    $("#loading").modal("show");
    $("#painel").load("monitor_adm.php");
    $("#loading").modal("hide");
}

*/

$(document).ready(function(){
    $('#ocultar_01').click(function(event){
        if($('#eyesUsuariosAdm').hasClass('glyphicon glyphicon-eye-close')){
            $('#eyesUsuariosAdm').attr('class','glyphicon glyphicon-eye-open');
        }else{
            $('#eyesUsuariosAdm').attr('class','glyphicon glyphicon-eye-close');
        }
    });

    $('#ocultar_01').click(function() {
        $('#tabela_01').toggle('slow');
    });

    $('#ocultar_02').click(function(event){
        if($('#eyesIntegracao').hasClass('glyphicon glyphicon-eye-close')){
            $('#eyesIntegracao').attr('class','glyphicon glyphicon-eye-open');
        }else{
            $('#eyesIntegracao').attr('class','glyphicon glyphicon-eye-close');
        }
    });
    $('#ocultar_02').click(function() {
        $('#tabela_02').toggle('slow');
    });


    $('#ocultar_03').click(function(event){
        if($('#eyesKronaOneAdm').hasClass('glyphicon glyphicon-eye-close')){
            $('#eyesKronaOneAdm').attr('class','glyphicon glyphicon-eye-open');
        }else{
            $('#eyesKronaOneAdm').attr('class','glyphicon glyphicon-eye-close');
        }
    });
    $('#ocultar_03').click(function() {
        $('#tabela_03').toggle('slow');
    });

})

//Filters Usuario (Monitor_ADM):
function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // vertical
    $dialog.css("margin-top", offset);
}

$('.modal').on('show.bs.modal', centerModal);
$(window).on("resize", function () {
    $('.modal:visible').each(centerModal);
});

