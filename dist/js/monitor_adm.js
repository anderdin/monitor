$(document).ready(function(){
    
    //Painel de cadastro de usuarios.
    $('.button_usuario').click(function(event){
        if($('#eyes_usuario').hasClass('glyphicon glyphicon-eye-open')){
            $('#eyes_usuario').attr('class','glyphicon glyphicon-eye-close');
        }else{
            $('#eyes_usuario').attr('class','glyphicon glyphicon-eye-open');
        }
    });

    $('#ocultar_AdmUsuario').click(function() {
        $('#adm_usuario').toggle('slow');
    });

    //FIM painel de cadastro de usuarios..






    //Painel de cadastro de monitores
    $('.button_monitor').click(function(event){
        if($('#eyes_monitor').hasClass('glyphicon glyphicon-eye-open')){
            $('#eyes_monitor').attr('class','glyphicon glyphicon-eye-close');
        }else{
            $('#eyes_monitor').attr('class','glyphicon glyphicon-eye-open');
        }
    });

    $('#ocultar_AdmMonitor').click(function() {
        $('#adm_monitor').toggle('slow');
    });
    //FIM painel de cadastro de Monitor.









    //Grupos
    $('.button_grupos').click(function(event){
        if($('#eyes_grupos').hasClass('glyphicon glyphicon-eye-open')){
            $('#eyes_grupos').attr('class','glyphicon glyphicon-eye-close');
        }else{
            $('#eyes_grupos').attr('class','glyphicon glyphicon-eye-open');
        }
    });

    $('#ocultar_AdmGrupos').click(function() {
        $('#adm_grupos').toggle('slow');
    });
    //FIM painel de cadastro de grupos.









    //Legendas
    $('#ocultar_LegenVelocidade').click(function(event){
        $('#leg_Velocidade').toggle('show');
    });

    $('.button_legenda_velocidade').click(function(event){
        if($('#eyesLegendaVelocidade').hasClass('glyphicon glyphicon-eye-open')){
            $('#eyesLegendaVelocidade').attr('class','glyphicon glyphicon-eye-close');
        }else{
            $('#eyesLegendaVelocidade').attr('class','glyphicon glyphicon-eye-open');
        }
    });
    
    //FIM LEGENDAS






    //HISTORICO
    $( document ).ready(function() {
    
    initEvents();
        
    });

    function initEvents() {
        
        $(".list").hover(function(){
            
            $(".list li:first span").stop().animate({borderWidth: "5", backgroundColor: "#3f3659", color: "#e5e3e8"},{duration: 170, complete: function() {}} ); 
            
        }, function () {
            
            $(".list li:first span").stop().animate({borderWidth: "2", backgroundColor: "#201c2b", color: "#b8b5c0"},{duration: 170, complete: function() {}} ); 

        });
        
    }

    function animateTable(e) { 
        
        
        
        
    }

    //FIM HISTORICO





    //Charts
    $('#ocultar_charts').click(function(){
        $('#charts').toggle('slow');
    });


});

//Funções
function cadastrarUsuarioGrupos(){

    var login = $('#userName').val();
    var email = $('#userEmail').val();
    var grupos = $('#userGrupo').val();
    var permissao = $('#userPermissao').val();
    var form = $('#cadastroUsuario').serializeArray();
    

    if(login !='' && email !='' && permissao !='' && grupos !='')
    {

        $(this).attr("disabled", "true");
        $("#userName").attr("disabled", "true");
        $("#userEmail").attr("disabled", "true");
        $("#userGrupo").attr("disabled", "true");
        $("#userPermissao").attr("disabled", "true");

        $(this).html("<img src='../dist/imagens/loading.gif' height='16px' width='16px'>");
        $.post("../controller/Monitor_admController.php", {operacao: "cadastrarUsuario", login:login, permissao:permissao, email:email, grupos:grupos}, function(a){
            alert('Usuário cadastrado com sucesso !');
            window.location.reload();
        },'text').fail(function(b,c,d){
            alert('Falha no cadastro.');
            window.location.reload();
           });
    }

}


function cadastrarMonitor(){

    var tecnologia = $('#cadastroMonitor').val();
    var grupos = $('#monitorGrupos').val();

    if(tecnologia !='' && grupos !=''){
        
        $("#cadastroMonitor").attr("disabled", "true");

        $(this).html("<img src='../dist/imagens/loading.gif' height='16px' width='16px'>");

        $.post("../controller/Monitor_admController.php", {operacao:"associacaoMonitor", tecnologia: tecnologia, grupos:grupos}, function(a){
            
                alert("Monitor Cadastrador com Sucesso");
                window.location.reload();
        },'json').fail(function(b,c,d){
           
            alert("Falha no Cadastro");
            window.location.reload();
           
        });
    }
}

function cadastrarGrupos(){

    var grupo = $('#grupoNome').val();

    if(grupo !=''){
        $(this).html("<img src='../dist/imagens/loading.gif' height='16px' width='16px'>");

        $.post("../controller/Monitor_admController.php", {operacao:"cadastrarGrupo", grupo:grupo}, function(a){
            alert('Grupo cadastrado com sucesso.');
            window.location.reload();
        },'json').fail(function(b,c,d){
            alert("Falha no cadastro");
            window.location.reload();
        });
    }
}
