<?php 
//Titulos
define('TITLE_HEADER','Monitor Krona de gravacao na Pamcary.');

define('KRONA_ONE_TITLE','Serviços de Integração de Tecnologias KronaOne');

define('MONITOR_USUARIO_TITLE','Usuário: ');

define('MONITOR_INTEGRACAO_TITLE','Serviços de Integração de Operações');

define('MONITOR_TITLE','SMS - MONITOR');

define('MONITOR_ADM_TITLE','Administração');

define('DASHBOARD_TITLE','DASHBOARD');

define('HISTORICO_TITLE','HISTÓRICO');
define('INDICE','ÍNDICE');


//Palavras
define('TECNOLOGIA', 'Tecnologia');

define('TECNOLOGIA_HISTORICO', 'Tecnologia');
define('TIPO_HISTORICO','TIPO');
define('NOTIFICACAO_HISTORICO','Notificação');
define('HORA_ENTRADA_HISTORICO','Hora de Entrada');
define('HORA_SAIDA_HISTORICO','Hora de Saída');
define('SOLUCIONADO_HISTORICO','Solucionado');

define('CONEXAO','Conexão');

define('ATRAZO','Atrazo');

define('VELOCIDADE','Velocidade');

define('OPERACAO', 'Operação');

define('QUANTIDADE','Qtde');

define('HISTORICO','Histórico');

define('ENTRADA','Entrada');
define('SAIDA','Saída');

define('ENVIAR','Enviar');
define('ENTRAR','ENTRAR');
define('LIMPAR','Limpar');

define('LOGOUT','logout');

define('MASTER','Master');
define('CLIENTE','Cliente');


//Frases
define('HORA_RASTREADOR','Hora do Evento no Rastreador');

define('ULTIMO_EVENT_K1','Último Evento Gravado no K-1');

define('ULTIMO_EVENT_KRONA','Último evento gravado na Krona');

define('HORA_EVENT_RASTREADOR','Hora do evento no rastreador');

define('HORA_EVENT_REGISTRO_PAMCARY','Hora do registro gravado na Pamcary');

define('HORA_EVENT_GRAVACAO_PAMCARY','Hora de gravação na Pamcary');

define('FALTA_TRANSMITIR','Falta Transmitir');

define('FALTA_REFERENCIA','Falta Referência');

define('QUANTIDADE_CARROS','Qtde. Carros');

define('HRA_ATUAL','Hora Atual');

define('PAINEL_NOTIFICACAO','Painel de Notificação.');

define('VER_TODOS_ALERTAS','Ver Todos Alertas');

define('ULTIMA_VIAGEM_CRIADA','Última Viagem Criada');

define('TIPO_USUARIO','Tipo de usuário');

define('REGISTRE_NOVA_SENHA','Por favor, registre uma nova senha.');

define('FRASE_ACESSO_LOGIN','Por favor, Faça o login em sua conta.');

define('CADASTRAR_GRUPO','Cadastrar Grupo');
define('CADASTRAR_MONITOR','Cadastrar Monitor');
define('CADASTRAR_USUARIO','Cadastrar Usuário');

//Menus

define('DASHBOARD','Dashboard');
define('ADMINISTRACAO','Administração');
define('GRUPOS','Grupos');
define('MONITOR','Monitor');



//Alertas
define('IRREGULARIDADE_SENHAS','As senhas não coincidem.');

define('CAMPOS_EM_BRANCO','Por favor, preencha os campos corretamente.');

define('ERRO','Erro, verifique se seu acesso está correto.');