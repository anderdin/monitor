<?php

/**
 * Description of Conexao
 * Arquivo que armazena todas a conexões utilizadas no sistema
 * @author Anderson
 */
class Conexao {
    
    function local(){
        $dns = "mysql:host=127.0.0.1;dbname=monitor_krona";
        $user = "root";
        $password = "";
        
        $conn = new PDO($dns, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        
        return $conn;
    }
    
}
