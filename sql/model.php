<?php
/*
* Todas e quaisquer alterações feitas no banco de dados devem ser registradas e armazenadas aqui.
* @Author Felipe Silva
*/


//Query que pega o id do usuario, atrelado a um grupo que também atrelado a um monitor, realizar joins e une para que um usuario possa ter um ou mais acessos baseado por seus grupos.

SELECT * FROM usuarios A 
INNER JOIN controle_grupos_usuarios B ON A.idusuarios = B.idcontrole_grupos_usuarios
INNER JOIN controle_monitores_grupo C ON B.controle_grupos_usuarios_id_grupo = C.controle_monitores_grupo_idGrupo
INNER JOIN monitores D ON D.idmonitores = C.controle_monitores_grupo_idMonitor
INNER JOIN grupos E ON E.idgrupos = B.controle_grupos_usuarios_id_grupo 
WHERE idusuarios = {$idusuarios} group by A.idusuarios; 

//Query
/*
* Autor: Felipe Silva
* Data: 24/04/2017
* Descrição: Criação da tabela monitor_grupos
* Em produção: Não.
* Revisado: Não
*/

CREATE TABLE `monitor_grupos` (
	`grupos_id` INT(11) NOT NULL AUTO_INCREMENT,
	`grupos_nome` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`grupos_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;

/*
* Autor: Felipe Silva
* Data: 24/04/2017
* Descrição: Criação da tabela monitor_usuarios
* Em produção: Não.
* Revisado: Não
*/
CREATE TABLE `monitor_usuarios` (
	`usuario_id` INT(11) NOT NULL AUTO_INCREMENT,
	`usuario_login` VARCHAR(255) NULL DEFAULT NULL,
	`usuario_senha` VARCHAR(255) NULL DEFAULT NULL,
	`usuario_permissao_nivel` INT(11) NULL DEFAULT NULL COMMENT '1= Master / 2= Cliente',
	`usuario_email` VARCHAR(50) NULL DEFAULT NULL,
	`usuario_grupo` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`usuario_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=3
;


/*
* Autor: Felipe Silva
* Data: 24/04/2017
* Descrição: Criação da tabela monitor_controle_grupos
* Em produção: Não.
* Revisado: Não
*/
CREATE TABLE `monitor_controle_grupos` (
	`monitor_controle_id` INT(11) NOT NULL AUTO_INCREMENT,
	`monitor_controle_grupos_usuario_id` INT(11) NULL DEFAULT NULL,
	`monitor_controle_grupos_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`monitor_controle_id`),
	INDEX `fk_usuario_idx` (`monitor_controle_grupos_usuario_id`),
	INDEX `fk_grupo_idx` (`monitor_controle_grupos_id`),
	CONSTRAINT `FK_ctlGruposUsuarios_grupo` FOREIGN KEY (`monitor_controle_id`) REFERENCES `monitor_grupos` (`grupos_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_ctlGruposUsuarios_usuario` FOREIGN KEY (`controle_grupos_usuarios_id_usuario`) REFERENCES `monitor_usuarios` (`usuarios_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;



/*
* Autor: Felipe Silva
* Data: 24/04/2017
* Descrição: Criação da tabela monitor_controle_monitores
* Em produção: Não.
* Revisado: Não
*/

CREATE TABLE `monitor_controle_monitores` (
	`controle_monitor_id` INT(11) NOT NULL AUTO_INCREMENT,
	`controle_monitores_grupo_idMonitor` INT(11) NULL DEFAULT '0',
	`controle_monitores_grupo_idGrupo` INT(11) NULL DEFAULT '0',
	PRIMARY KEY (`controle_monitor_id`),
	INDEX `FK_controle_monitor_grupo_monitor` (`controle_monitores_grupo_idMonitor`),
	INDEX `FK_controle_monitor_grupo_grupo_` (`controle_monitores_grupo_idGrupo`),
	CONSTRAINT `FK_controle_monitor_grupo_grupo_` FOREIGN KEY (`controle_monitores_grupo_idGrupo`) REFERENCES `monitor_grupos` (`grupos_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_controle_monitor_grupo_monitor` FOREIGN KEY (`controle_monitores_grupo_idMonitor`) REFERENCES `monitores` (`idmonitores`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=6
;



/*
* Autor: 
* Data: 
* Descrição: 
* Em produção: Não.
* Revisado: Não
*/






