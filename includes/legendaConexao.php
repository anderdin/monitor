<div class="panel panel-info box-shadow">
    <div class="panel-heading ">
        <i class="fa fa-quote-left fa-fw"></i><b><?php echo CONEXAO;?></b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_usuario" id="ocultar_AdmUsuario"><span class="glyphicon glyphicon-eye-open" id="eyes_usuario"></span></button>
        </div>
    </div>

    <div class="panel-body" id="adm_usuario" style="display: none;">
        <div class="col-lg-6">
            <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Offline">
        </div>
        <div class="col-lg-6"><b>Offline.</b></div>

        <div class="col-lg-6">
            <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
        </div>
        <div class="col-lg-6"><b>Ocilante.</b></div>

        <div class="col-lg-6">
            <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
        </div>
        <div class="col-lg-6"><b>Online.</b></div>
    </div>
    <!-- /.panel-body -->
</div>