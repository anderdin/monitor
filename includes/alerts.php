<!-- Alertas da página confirmarSenha.php -->
<div class="alert alert-danger center" id="alertsConfirmarSenha" style="display: none;">
  As senhas não coincidem. <a href="#" class="alert-link"></a>
</div>
<!-- FIM Alertas confirmarSenha.phg -->

<!-- Alertas da página index.php -->
<div class="alert alert-warning center" id="alertsLoginWarning" style="display: none;">
Por favor, preencha os campos corretamente.<a href="#" class="alert-link"></a>
</div>

<div class="alert alert-danger center" id="alertsLoginErro" style="display: none;">
Erro, verifique se seu acesso está correto.<a href="#" class="alert-link"></a>
</div>

<!-- Fim Alertas index -->


<!-- Alerta de sessão finalizada -->

	
<!-- Fim Alerta Sessão Finalizada-->