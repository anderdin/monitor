<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3><?php echo KRONA_ONE_TITLE; ?></h3>
                            <div style="float:right; margin-top: -35px;" >
                                <button class="btn btn-default btn-xs" id="ocultar_03">
                                    <span class="glyphicon glyphicon-eye-close" id="eyesKronaOneAdm"></span>
                                </button>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-xs btn-filter" onclick="KronaOne();"><span class="glyphicon glyphicon-filter" data-toggle="modal" data-target="#myModalKronaOne" ></span></button>
                                </div>
                                <?php require "../includes/filters/filterKronaOneAdm.php";?>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="tabela_03">
                                    <thead>
                                        <tr>
                                            <th class="center"><?php echo TECNOLOGIA;?></th>
                                            <th class="center"><?php echo HORA_RASTREADOR;?></th>
                                            <th class="center"><?php echo ULTIMO_EVENT_K1;?></th>
                                            <th class="center"><?php echo HRA_ATUAL;?></th>
                                             <th class="center">
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                             <th class="center">
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                             <th class="center">
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($monitorKrona as $key => $kronaOne){?>
                                        <tr>
                                            <td class="center"><?php echo $kronaOne['tecnologia'];?></td>
                                            <td class="center"><?php echo $kronaOne['hora_evento'];?></td>
                                            <td class="center"><?php echo $kronaOne['ultimo_evento'];?></td>
                                            <td class="center"><?php  $date = date_create($kronaOne['DATA_ATUAL']); echo date_format($date, 'H:i:s d/m'); ?></td>
                                            
                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Muito">
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Muito">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>