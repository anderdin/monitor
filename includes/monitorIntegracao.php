<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3><?php echo MONITOR_INTEGRACAO_TITLE;?></h3>
                            <div style="float:right; margin-top: -35px;" >
                                <button class="btn btn-default btn-xs" id="ocultar_02">
                                    <span class="glyphicon glyphicon-eye-close" id="eyesIntegracao"></span>
                                </button>

                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-xs btn-filter" onclick="Operacoes();">
                                        <span class="glyphicon glyphicon-filter" data-toggle="modal" data-target="#myModalOperacoes"></span>
                                    </button>
                                </div>
                                <?php require "../includes/filters/filterOperacoes.php";?>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table  class="table table-striped" id="tabela_02" >
                                    <thead>
                                        <tr>
                                            <th class="center">Operação</th>
                                            <th class="center">Última Viagem Criada</th>
                                            <th class="center">Qtde</th>
                                            <th class="center">Hora Atual</th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                <?php echo ENTRADA;?>
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                <?php echo SAIDA;?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    //Visualização dos dados da tabela Monitor.
                                    foreach($monitorViagens as $key => $Viagens):?>
                                        <tr>
                                            <td class="center">
                                                <?php echo $Viagens['operacao']; ?>
                                            </td>
                                            
                                            <td class="center">
                                                <?php $dateRegistro = date_create($Viagens['ultimo_registro']); echo date_format($dateRegistro,'H:i:s d/m'); ?>
                                            </td>
                                            <td class="center">
                                                <?php if($Viagens['viagens_qtde']<=0){ echo "Sem Novas Viagens";}else{ echo $Viagens['viagens_qtde'];} ?>
                                            </td>

                                            <td class="center">
                                                <?php $dateTime = date('H:i:s'); echo $dateTime;?>
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>

                                            <td class="center">
                                                <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Muito">
                                            </td>
                                            
                                            <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
     