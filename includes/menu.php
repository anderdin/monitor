<?php 

require "../model/Usuarios.php";
$usuario = new usuarios();
$usuario->idusuarios = $_SESSION['id_usuario'];

$usuarioGrupo = $usuario->buscarTodos($usuario);

if($_SESSION['UsuarioNivel'] == 1){
        $usu_nivel = "Master";
 }
 if($_SESSION['UsuarioNivel'] != $Usuario_Master){
        $usu_nivel = "Cliente";
}

?>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- class="navbar-brand" -->
                <a href="index.php"> <img src="../dist/imagens/logo_krona.png" ></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        
                        <i class="fa fa-bell fa-fw"></i>
                            <?php if($notificacao > 0){?>
                                <span class="badge">
                                    <?php echo $notificacao; ?>
                                </span>
                            <?php } ?> 
                        <i class="fa fa-caret-down"></i>
                        
                    </a>
                    <ul class="dropdown-menu dropdown-alerts left">
                        <li>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-warning fa-fw"></i> Server Not Responding
                                <span class="pull-right text-muted small"><em>10:57 AM</em>
                                </span>
                            </a>
                        </li>
                            
           
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i><?php echo $_SESSION['login']; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user left-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <i><?php echo $usu_nivel; ?></i></a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="config.php"><i class="fa fa-cogs fa-fw"></i> <?php echo 'Configurações'; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li><a href='../controller/LoginController.php?operacao=logoff'><i class="fa fa-sign-out fa-fw"></i><?php echo LOGOUT; ?></a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard "></i> <?php echo DASHBOARD; ?>  <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dashboard.php"><i class="fa fa-desktop fa-fw"></i> Monitoramento</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>  Cone de Risco</a>
                                </li>
                            </ul>                                 
                        </li>
                        <?php 
                            if($_SESSION['UsuarioNivel'] == $Usuario_Master){
                        ?>
                        <li>
                            <a href="Monitor_adm.php"><i class="fa fa-folder-open"></i> <?php echo ADMINISTRACAO; ?></a>
                        </li>
                         
                        <?php }?>
                        <li>
                            <a href="Monitor.php"><i class="fa fa-desktop fa-fw"></i> <?php echo MONITOR; ?></a>
                        </li>
                         
                        <li>
                            <a href="#"><i class="fa  fa-group fa-fw"></i> <?php echo GRUPOS;?> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <?php foreach ($usuarioGrupo as $key => $empresas): ?>
                                <li>
                                    <a href="#"><?php echo $empresas['grupos_nome'];?></a>
                                </li>
                                <?php endforeach ?>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>