<div class="panel panel-info box-shadow">
    <div class="panel-heading ">
       <i class="fa fa-group fa-fw"></i> <b><?php echo  CADASTRAR_GRUPO; ?></b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_grupos" id="ocultar_AdmGrupos"><span class="glyphicon glyphicon-eye-open" id="eyes_grupos"></span></button>
        </div>
    </div>

    <div class="panel-body" id="adm_grupos" style="display: none;">
        <div class="form-group input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            <input type="text" class="form-control" placeholder="Grupo" id="grupoNome">
        </div>

   
        <button onclick="cadastrarGrupos();" class="btn btn-default"><?php echo ENVIAR; ?></button>
        <button type="reset" class="btn btn-default"><?php  echo LIMPAR;?></button>
    </div>
    <!-- /.panel-body -->
</div>