<div class="panel panel-black box-shadow">
    <div class="panel-heading ">
      <i class="fa fa-quote-left fa-fw"></i><b> Atrazo</b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_monitor" id="ocultar_AdmMonitor"><span class="glyphicon glyphicon-eye-open" id="eyes_monitor"></span></button>
        </div>
    </div>

    <div class="panel-body" id="adm_monitor" style="display: none;">
        <div class="col-lg-6">
            <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Offline">
        </div>
        <div class="col-lg-6"><b>Muito.</b></div>
   
        <div class="col-lg-6">
            <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
        </div>
        <div class="col-lg-6"><b> Mediano.</b></div>
   
        <div class="col-lg-6">
            <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
        </div>
        <div class="col-lg-6"><b>Pouco.</b></div>
    </div>
    <!-- /.panel-body -->
</div>