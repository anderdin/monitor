<div class="panel panel-info box-shadow">
    <div class="panel-heading ">
        <i class="fa fa-quote-left fa-fw"></i><b> Velocidade</b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_legenda_velocidade" id="ocultar_LegenVelocidade"><span class="glyphicon glyphicon-eye-open" id="eyesLegendaVelocidade"></span></button>
        </div>
    </div>

    <div class="panel-body" id="leg_Velocidade" style="display: none;">
        <div class="col-xs-6">
            <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Offline">
        </div>
        <div class="col-lg-6"><b>Devagar.</b></div>

        <div class="col-lg-6">
            <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
        </div>
        <div class="col-lg-6"><b>Média.</b></div>

        <div class="col-lg-6">
            <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
        </div>
        <div class="col-lg-6"><b>Boa.</b></div>
    </div>
    <!-- /.panel-body -->
</div>