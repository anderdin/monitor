<?php
$url = $_SERVER['PHP_SELF'];
$caminho = "/monitor-2.0/views/index.php";

  if($url != $caminho)
    {
        if ($notificacao >= 1)
        { 
?>
    <div class="col-lg-4 notificationFixed" id="fechar_notificacao" ><!--PAINEL DE NOTIFICAÇÃO -->
        <div class="panel panel-red box-shadow">

            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Painel de Notificação. <span class="badge"><?php echo $notificacao;?></span> 
                <div style="float:right;" >
                    <button class="btn btn-default btn-xs button_notificacao" id="ocultar_notificacao"><span class="glyphicon glyphicon-eye-open" id="eyes_notificacao" ></span></button>

                    <div id="fechar"><a href="#" title="Fechar" class="fechar"><span class="fa fa-times-circle-o"></span></a></div>
                </div>
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body" id="notificacao" style="display: none;">
                <div class="list-group">
                    <?php for($i=1; $i<=7; $i++){?>                                   
                    <a href="#" class="list-group-item">
                        <i class="fa fa-warning fa-fw"></i> Server Not Responding
                        <span class="pull-right text-muted small"><em>10:57 AM</em>
                        </span>
                    </a>
                    <?php } ?>
                    
                </div>
                <!-- /.list-group -->
                <a href="#" class="btn btn-default btn-block">Ver Todos Alertas</a>
            </div>
        </div>
    </div><!-- FIM PAINEL DE NOTIFICAÇÃO -->

<?php
        }
    }
?>