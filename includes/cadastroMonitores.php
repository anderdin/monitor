<?php
require_once "../model/Monitor.php";


$monitor = new Monitor();

$monitores = $monitor->buscarTodos();


?>
<div class="panel panel-black box-shadow">
    <div class="panel-heading ">
      <i class="fa fa-desktop fa-fw"></i> <b><?php echo CADASTRAR_MONITOR;?></b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_monitor" id="ocultar_AdmMonitor"><span class="glyphicon glyphicon-eye-open" id="eyes_monitor"></span></button>
        </div>
    </div>

    <div class="panel-body" id="adm_monitor" style="display: none;">
        <div class="form-group input-group">
            <span class="input-group-addon"><span class="fa fa-desktop fa-fw"></span></span>
           
            <!-- <select class="form-control" name="userGrupo" id="userGrupo">
                <?php foreach($monitores as $key =>$nomeMonitores){?>
                <option value="<?php echo $nomeMonitores['idmonitores']?>">
                    <?php echo $nomeMonitores['monitores_tecnologia']; ?>
                </option>
                <?php } ?>
            </select>-->

            <select type="text" name="cadastroMonitor" id="cadastroMonitor" class="form-control multiselect multiselect-icon" multiple="multiple" role="multiselect">          
                     <?php foreach($monitores as $key =>$nomeMonitores){?>
                    <option value="<?php echo $nomeMonitores['idmonitores']?>">
                        <?php echo $nomeMonitores['monitores_tecnologia']; ?>
                    </option>
                    <?php } ?>
            </select>
           
        </div>
        <div class="form-group input-group">
            <span class="input-group-addon"><span class="fa fa-group fa-fw"></span></span>
           
            <select type="text" name="monitorGrupos" id="monitorGrupos" class="form-control multiselect multiselect-icon" multiple="multiple" role="multiselect">          
                    <?php foreach($grupos as $key =>$nomeGrupos){?>
                    <option value="<?php echo $nomeGrupos['idgrupos']?>">
                        <?php echo $nomeGrupos['grupos_nome']; ?>
                    </option>
                    <?php } ?>
            </select>

           
        </div>
        <h6><smaal>* Monitor será atrelado ao grupo escolhido.</smaal></h6>
        <br>
        <button onclick="cadastrarMonitor();" class="btn btn-default"><?php echo ENVIAR;?></button>
        <button type="reset" class="btn btn-default"><?php echo LIMPAR;?></button>
    </div>
    <!-- /.panel-body -->
</div>