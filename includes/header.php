<?php
require_once "../constantes/constantes.php";

// USAR LOGICA PARA QUANDO HOUVER UMA NOTIFICAÇÃO
$notificacao = '5';

if($notificacao > 0){?>

<script>    
    alert("Notificação !!!!!!!!!!!!!!!!!");
</script>

<?php 
}
$Usuario_Master = '1';
 
    session_start();
    if(!isset($_SESSION['login'])){
       header("Location:/monitor-2.0/views/");
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../dist/imagens/favicon.ico" type="image/x-icon" />

    <meta name="description" content="">
    <meta name="author" content="Felipe Feitosa da Silva">

    <title><?php echo TITLE_HEADER; ?> </title>
   
    <!-- JS -->
    <script src="../dist/js/Login.js"></script>
    <script src="../js/highcharts.js"></script>
    

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/funnel.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/principal.css" rel="stylesheet">
    
    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    