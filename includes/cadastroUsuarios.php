<?php
require_once "../model/Grupos.php";
require_once "../model/Monitor.php";

$gruposNome = new Grupos();
$grupos = $gruposNome->buscarTodos();

$monitorNome = new Monitor();
$monitor = $monitorNome->buscarTodos();

?>
<div class="panel panel-info box-shadow">
    <div class="panel-heading ">
        <i class="fa fa-user fa-fw"></i><b><?php echo CADASTRAR_USUARIO; ?></b>
        <div style="float:right;" >
            <button class="btn btn-default btn-xs button_usuario" id="ocultar_AdmUsuario"><span class="glyphicon glyphicon-eye-open" id="eyes_usuario"></span></button>
        </div>
    </div>

    <div class="panel-body" id="adm_usuario" style="display: none;">
            <div class="form-group input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control" placeholder="Username" name="userName" id="userName">
            </div>
            <div class="form-group input-group">
                <span class="input-group-addon">
                    <span class="fa fa-envelope-o"></span>
                </span>
                <input type="email" class="form-control" placeholder="Email" name="email" id="userEmail">
            </div>
            <div class="form-group">
                <label><?php echo TIPO_USUARIO; ?></label>
                <select class="form-control" name="userPermissao" id="userPermissao">
                    <option value="1"><?php echo MASTER; ?></option>
                    <option value="2"><?php echo CLIENTE; ?></option>
                </select>
            </div>
            <div class="form-group">
                <label><?php echo GRUPOS.':'; ?></label>
               
                <!-- <select class="form-control" name="userGrupo" id="userGrupo">
                    <?php foreach($grupos as $key =>$nomeGrupos){?>
                    <option value="<?php echo $nomeGrupos['idgrupos']?>">
                        <?php echo $nomeGrupos['grupos_nome']; ?>
                    </option>
                    <?php } ?>
                </select>-->

                <select type="text" name="userGrupo" id="userGrupo" class="form-control multiselect multiselect-icon" multiple="multiple" role="multiselect">          
                    <?php foreach($grupos as $key =>$nomeGrupos){?>
                    <option value="<?php echo $nomeGrupos['idgrupos']?>">
                        <?php echo $nomeGrupos['grupos_nome']; ?>
                    </option>
                    <?php } ?>
                </select>
            </div>

            <button onclick="cadastrarUsuarioGrupos();" class="btn btn-default">
                <?php echo ENVIAR; ?>
            </button>
            <button type="reset" class="btn btn-default"><?php echo LIMPAR; ?></button>
    </div>
    <!-- /.panel-body -->
</div>
