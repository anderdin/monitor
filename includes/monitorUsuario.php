<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                <h3><?php echo MONITOR_USUARIO_TITLE; echo $_SESSION['login']; ?></h3>
                            <div style="float:right; margin-top: -35px;" >
                                <button class="btn btn-default btn-xs" id="ocultar_01">
                                    <span class="glyphicon glyphicon-eye-close" id="eyesUsuariosAdm"></span> 
                                </button>

                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-xs btn-filter" onclick="centerModal();"><span class="glyphicon glyphicon-filter" data-toggle="modal" data-target="#myModal" ></span></button>
                                </div>
                                <?php 
                                require "../includes/filters/filterUsuariosAdm.php";
                                ?>
                            </div>

                        </div> 
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="tabela_01" >
                                    <thead>
                                        <tr>
                                  
                                            <th class="center">Tecnologia</th>
                                            <th class="center">Falta Transmitir</th>
                                            <th class="center">Falta Referência</th>
                                            <th class="center">Qtde. Carros</th>
                                            <th class="center"> 
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                Entrada 
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Conexão)</b></small>
                                                </h5> 
                                                Saída
                                            </th>

                                            <th class="center"> 
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                Entrada 
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Atrazo)</b></small>
                                                </h5> 
                                                Saída
                                            </th>

                                            <th class="center"> 
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                Entrada 
                                            </th>
                                            <th class="center">
                                                <h5>
                                                    <small><b>(Velocidade)</b></small>
                                                </h5> 
                                                Saída
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($monitores as $key => $Tecnologia):?>
                                        <tr>
                                            <td class="center">
                                                <?php echo $Tecnologia['monitores_tecnologia'];?>
                                            </td>
                                            <td class="center"><?php  echo $Tecnologia['monitores_transmitir'];?></td>
                                            <td class="center"><?php  echo $Tecnologia['monitores_transmitir'];?></td>
                                            <td class="center"><?php  echo $Tecnologia['monitores_qtda_veiculos'];?></td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>   
                                            <td class="center">
                                                 <img src="../dist/imagens/farol_verde.png" width="50" alt="semafaro" title="Online">
                                            </td>                                          
                                            <td class="center">
                                                <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Muito">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_vermelho.png" width="50" alt="semafaro" title="Muito">
                                            </td>
                                            <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                             <td class="center">
                                                <img src="../dist/imagens/farol_amarelo.png" width="50" alt="semafaro" title="Ocilante">
                                            </td>
                                        </tr>

                                    <?php endforeach ?>
                                     
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>