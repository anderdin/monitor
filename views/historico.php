<?php 
    require "../includes/header.php";
    require "../includes/menu.php";
    require "../model/Monitor.php";


    date_default_timezone_set('America/Sao_Paulo');

?>
        <div id="page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" style="text-shadow:2px 2px 8px grey;">
                        <h1 class="page-header"><i class="fa fa-book fa-fw"></i> <b><?php echo HISTORICO_TITLE;?></b></h1>
                    </div>
                </div>

                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3><?php echo INDICE; ?></h3>
                                <div style="float:right; margin-top: -35px;" >
                                    <button class="btn btn-default btn-xs" id="ocultar_01">
                                        <span class="glyphicon glyphicon-eye-close" id="eyesUsuariosAdm"></span> 
                                    </button>
                                </div>
                            </div> 
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="tabela_01" >
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="center"></td>
                                            </tr>

                                       
                                         
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
               
            </div>
            <!-- /#page-wrapper -->
        </div>

<?php
require "../includes/footer.php";
?>
