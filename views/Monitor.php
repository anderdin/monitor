<?php 
    require "../includes/header.php";
    require "../includes/menu.php";
    require "../model/Monitor.php";
    require "../model/MonitorViagens.php";
    require "../model/MonitorKrona.php";

    date_default_timezone_set('America/Sao_Paulo');

    //Chamada da Classe Monitor e função buscarTodos.
    $res = new Monitor();
    $monitores = $res->buscarMonitoresUsuario();


    //Chamada da Classe MonitorViagens e Função buscarTodos.
    $res1 = new MonitorViagens();
    $monitorViagens = $res1->buscarTodos();


    //Chamada da Classe MonitorKrona1 e Função buscarTodos.
    $res2 = new MonitorKrona1();
    $monitorKrona = $res2->buscarTodos();
?>
          <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12" style="text-shadow:2px 2px 8px grey;">
                    <h1 class="page-header"><i class="fa fa-desktop fa-fw"></i> <b><?php echo MONITOR_TITLE;?></b></h1>
                </div>
            </div>

            <div class="centralizar" >  
                <div class="col-lg-3 center">
                    <?php require "../includes/legendaConexao.php"; ?>
                </div>
                <div class="col-lg-3 center">
                    <?php require "../includes/legendaAtrazo.php"; ?>
                </div>
                <div class="col-lg-3 center">
                    <?php require "../includes/legendaVelocidade.php"; ?>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <?php require '../includes/monitorUsuario.php';?>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <?php  require '../includes/monitorIntegracao.php';?>
            </div>


            <div class="row">
                <?php  require '../includes/monitorKronaOne.php';?>

                <div class="center">VEICULOS SE COMUNICANDO COM A KRONA <b><i>OMNILINK:</i> 125 <i>SASCAR:</i> 363 <i>ONIX:</i> 268   <i>STI:</i> 0 <i>AUTOTRAC:</i> 335   <i>OMNILINK:</i> 159 <i>SIGHRA:</i> 34</b> 


                </div>
         
            </div>
            <!-- /#page-wrapper -->
        </div>

<?php
require "../includes/footer.php";
?>
