<?php 
    require "../includes/header.php";
    require "../includes/menu.php";
?>
<div id="page-wrapper">
  <div class="container-fluid">
      
    <div class="row">
        <div class="col-lg-12" style="text-shadow:2px 2px 8px grey;">
            <h1 class="page-header"><i class="fa fa-dashboard "></i> <b><?php echo DASHBOARD_TITLE; ?></b></h1>
        </div>
    </div>
      
		<div class="row">
      <div class="col-md-12">
        <div class="panel-heading">
          <i class="fa fa-book fa-fw"></i> <b><?php echo HISTORICO_TITLE;?></b>
        </div>

        <div class="listWrap">
        
            <ul class="list">
                <li>
                    <span><?php echo TECNOLOGIA_HISTORICO;?></span>
                    <span><?php echo NOTIFICACAO_HISTORICO;?></span>
                    <span><?php echo HORA_ENTRADA_HISTORICO;?></span>
                    <span><?php echo HORA_SAIDA_HISTORICO;?></span>
                    <span><?php echo TIPO_HISTORICO; ?> </span>
                </li>
                <li>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span><span class="label label-danger"><?php echo CONEXAO;?></span></span>
                </li>
                <li>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span><span class="label label-success"><?php echo ATRAZO;?></span></span>
                </li>
                <li>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span><span class="label label-warning"><?php echo VELOCIDADE; ?></span></span>
                </li>
                <li>
                </li>
            </ul>
        </div>   
      </div>

      <br /><br />
      
      <div class="col-md-12">
        
        <div class="panel panel-warning">
          <div class="panel-heading">
            <h3 class="panel-title">
              Nem Todos os Sistema Operacinais
              <small class="pull-right">Atualizado 39 minutes atrás</small>
            </h3>
          </div>                
        </div>
                          
        <div class="row clearfix">
            <div class="col-md-12 column">
                <div class="list-group">
                  
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Website and API 
                            <a href="#"  data-toggle="tooltip" data-placement="bottom" title="Access website and use site API">
                              <i class="fa fa-question-circle"></i>
                            </a>
                        </h4>
                        <p class="list-group-item-text">
                            <span class="label label-danger">Not Operational</span>
                        </p>
                    </div>
                  
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            SSH 
                            <a href="#"  data-toggle="tooltip" data-placement="bottom" title="Access site using SSH terminal">
                              <i class="fa fa-question-circle"></i>
                            </a>
                        </h4>
                        <p class="list-group-item-text">
                            <span class="label label-success">Operational</span>
                        </p>
                    </div>
                  
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Database Server 
                            <a href="#"  data-toggle="tooltip" data-placement="bottom" title="Access database server and execute queries">
                              <i class="fa fa-question-circle"></i>
                            </a>
                        </h4>
                        <p class="list-group-item-text">
                            <span class="label label-success">Operational</span>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
require "../includes/footer.php";
?>