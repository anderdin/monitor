<script type="text/javascript">
history.pushState(null, null, location.href);
    window.onpopstate = function(event) {
        history.go(1);
    };
</script>
<?php require "../includes/header.php";?>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <div class="center">
                <img src="../dist/imagens/logo_krona.png">
              </div>
                <div class="login-panel panel panel-info mainbox ">
                    <div class="panel-heading">
                        <h4 class="center" style="font-size:15px;"><?php echo REGISTRE_NOVA_SENHA;?></h4>
                    </div>
                    <div class="panel-body">
                        
                        <fieldset>
                            <input id="idUsuario" class="form-control" placeholder="Password" name="senha" type="hidden" value="<?php echo $_SESSION['idUsuario']?>">
                            <div class="form-group">
                                <input id="senha" class="form-control" placeholder="Senha" name="senha" type="password" value="">
                            </div>
                            <div class="form-group">
                                <input id="senhaConfirmacao" class="form-control" placeholder="Confirmar senha" name="senha" type="password" value="">
                            </div>
                          
                            <!-- Change this to a button or input when using this as a form -->
                            <button onclick="loginSenha();" id="btnEntrar" class="btn btn-lg btn-success btn-block"><?php echo ENTRAR;?></button>
                        </fieldset>
                    </div>
                </div>
                <?php  require "../includes/alerts.php";?>
            </div>
        </div>
    </div>

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="../data/flot-data.js"></script>
    
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/monitor.js"></script>
    <script src="../dist/js/monitor_adm.js"></script>
    <script src="../dist/js/notificacao.js"></script>




</body>

</html>
