<?php
require_once "../includes/header.php";
require_once "../includes/menu.php";
require_once "../model/Usuarios.php";
require_once "../model/Grupos.php";

$usuario = new Usuarios();
$grupos = new Grupos();

$usuario->idusuarios = $_SESSION['id_usuario'];

$grupos  = $grupos->buscarTodos(); 
$confUsuario = $usuario->confiUsuario($usuario);

$confGrupos  = $usuario->confiGruposUsuarios($usuario);

?>
<div id="page-wrapper">
    <div class="row">
    	<div class="col-lg-12" style="text-shadow:2px 2px 8px grey;">
            <h1 class="page-header">
            	<i class="fa fa-cogs fa-fw"></i> 
            	<b><?php echo "Configurações";?></b>
            </h1>
        </div>
    </div>
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-md-6">
	    		<div class="panel panel-info box-shadow">
	                <div class="panel-heading">
	                    <i class="fa fa-user fa-fw"></i> 
	                    Dados Pessoais
	                </div>
	                <div class="panel-body">
	                <?php foreach($confUsuario as $key =>$dadosUsuario){?>
	                    
	                     <div class="form-group input-group">
				            <input type="hidden" class="form-control" name="idusuarios" id="idusuarios" value="<?php echo $dadosUsuario['idusuarios'];?>">
				        </div>
	                    <div class="form-group input-group">
				            <span class="input-group-addon">@</span>
				            <input type="text" class="form-control" placeholder="Username" name="userName" id="userName" value="<?php echo $dadosUsuario['usuarios_login'];?>">
				        </div>

				         <div class="form-group input-group">
				            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
				            <input type="password" class="form-control" placeholder="Senha" name="senha" id="userSenha" title="Senha" value="<?php echo $dadosUsuario['usuarios_senha'];?>">

				            <input type="password" class="form-control" placeholder="Confirmar senha" name="confSenha" title="Confirmar Senha" id="confSenha" value="<?php echo $dadosUsuario['usuarios_senha'];?>">
				        </div>

				        <div class="form-group input-group">
				            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>

				            <input type="text" class="form-control" placeholder="E-mail" name="userEmail" id="userEmail" value="<?php echo $dadosUsuario['usuarios_email'];?>">
				        </div>

				        <div class="form-group">
				            <label><?php echo TIPO_USUARIO; ?></label>
				            <select class="form-control" name="permissao" id="userPermissao">
				            	<?php if($_SESSION['UsuarioNivel'] ==1){?>
				                	<option value="1" selected><?php echo MASTER; ?></option>
				                	<option value="2"><?php echo CLIENTE; ?></option>
				               <?php }else{  ?>
				                	<option value="2" selected><?php echo CLIENTE; ?></option>
				                <?php } ?>
				            </select>
				        </div>

				        <!--<div class="form-group">
				            <label><?php echo 'Grupo'; ?></label>
				            <select class="form-control" name="permissao" id="userPermissao">
				            	<?php if($_SESSION['UsuarioNivel'] ==1){?>
				                	<option value="1" selected><?php echo MASTER; ?></option>
				                	<option value="2"><?php echo CLIENTE; ?></option>
				               <?php }else{  ?>
				                	<o,ption value="2" selected><?php echo CLIENTE; ?></option>
				                <?php } ?>
				            </select>
				        </div>-->

				    <?php }  ?>
					    <div class="form-group">
			                <label><?php echo GRUPOS.':'; ?></label>
			               
			               <!-- <select class="form-control" name="userGrupo" id="userGrupo">
			                    <?php foreach($grupos as $key =>$nomeGrupos){?>
			                    <option value="<?php echo $nomeGrupos['idgrupos']?>">
			                        <?php echo $nomeGrupos['grupos_nome']; ?>
			                    </option>
			                    <?php } ?>
			                </select>-->

			                <select type="text" name="userGrupo" id="userGrupo" class="form-control multiselect multiselect-icon" multiple="multiple" role="multiselect">          
			            	<?php foreach($confGrupos as $grupoSelecionado){ ?>	
	                    			<option value="<?php echo $grupoSelecionado['idgrupos']?>" selected>
				                        <?php echo $grupoSelecionado['grupos_nome']; ?>
				                        
				                    </option>
			                    <?php } ?>
			                </select>
			            </div>
	                </div>

	                <div class="panel-footer">
	                   <button onclick="atualizarUsuario();" class="btn btn-default">
            				<?php echo 'Atualizar'; ?>
        				</button>
        				<button type="reset" class="btn btn-default"><?php echo "Cancelar"; ?></button>
	                </div>
	            </div>
	            <?php require_once "../includes/alerts.php";?>
	        </div>
	        <?php if($_SESSION['UsuarioNivel'] == 1){?>
	        <div class="col-md-6">
		        <div class="panel panel-black box-shadow">
				    <div class="panel-heading ">
				      <i class="fa fa-desktop fa-fw"></i> <b><?php echo CADASTRAR_MONITOR;?></b>
				        <div style="float:right;" >
				            <button class="btn btn-default btn-xs button_monitor" id="ocultar_AdmMonitor"><span class="glyphicon glyphicon-eye-open" id="eyes_monitor"></span></button>
				        </div>
				    </div>

				    <div class="panel-body" id="adm_monitor" style="display: none;">
				        <div class="form-group input-group">
	            			<span class="input-group-addon"><span class="fa fa-desktop fa-fw"></span></span>
							<input type="text" class="form-control" placeholder="Tecnologia" name="cadTecnologia" id="cadTecnologia">    
				        </div>
				        
				        <button onclick="cadastrarMonitor();" class="btn btn-default"><?php echo ENVIAR;?></button>
				        <button type="reset" class="btn btn-default"><?php echo LIMPAR;?></button>
				    </div>
				    <!-- /.panel-body -->
				</div>
			</div>
			<?php }?>

	        <div class="col-md-6" style="display: none;">
	    		<div class="panel panel-black box-shadow">
	                <div class="panel-heading">
	                    <i class="fa fa-group fa-fw"></i> 
	                    Usuarios
	                </div>
	                <div class="panel-body">
	                   
	                </div>
	                <div class="panel-footer">
	                    
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

<?php
require "../includes/footer.php";
?>
