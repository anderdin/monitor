<?php 
    require "../includes/header.php";
    require "../includes/menu.php";
?>
        <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" style="text-shadow:2px 2px 8px grey;">
                    <h1 class="page-header"><i class="fa fa-folder-open"></i> <b><?php echo MONITOR_ADM_TITLE; ?></b></h1>
                </div>
            </div>
            <div class="row"> 
                <div class="col-lg-4"><!-- PAINEL DE CADASTRO DE USUARIOS -->
                    <?php require "../includes/cadastroUsuarios.php";?>
                </div><!-- FIM PAINEL DE CADASTRO DE USUARIOS -->

                <div class="col-lg-4"><!-- PAINEL DE CADASTRO DE MONITORES -->
                    <?php require "../includes/cadastroMonitores.php";?>
                </div><!-- FIM PAINEL DE CADASTRO DE MONITORES -->

                <div class="col-lg-4"><!-- PAINEL DE CADASTRO DE MONITORES -->
                    <?php require "../includes/cadastroGrupos.php";?>
                </div><!-- FIM PAINEL DE CADASTRO DE MONITORES -->
            </div>
    
            <div class="row">
                

            </div>
          
        </div>
    </div>
    <!-- /.container-fluid -->

        <!-- /#page-wrapper -->
<?php
require "../includes/footer.php";
?>
