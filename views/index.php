<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../dist/imagens/favicon.ico" type="image/x-icon" />

    <meta name="description" content="">
    <meta name="author" content="Felipe Feitosa da Silva">

    <title>Monitor Krona de gravacao na Pamcary </title>
   
    <!-- JS -->
    <script src="../dist/js/Login.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../dist/css/principal.css" rel="stylesheet">
    
    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
body{
    background-image: url("../dist/imagens/background-login.jpg");
}

</style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <div class="center">
                <img src="../dist/imagens/logo_krona.png">
              </div>
                <div class="login-panel panel panel-info mainbox ">
                    <div class="panel-heading">
                        <h4 class="center" style="font-size:15px;">Por favor, Faça o login em sua conta.</h4>
                    </div>
                    <div class="panel-body">
                        
                        <fieldset>
                            <div class="form-group">
                                <input id="login" class="form-control" placeholder="Username" name="login" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input id="senha" class="form-control" placeholder="Password" name="senha" type="password" value="">
                            </div>
                          
                            <!-- Change this to a button or input when using this as a form -->
                            <button onclick="login();" id="btnEntrar" class="btn btn-lg btn-success btn-block">ENTRAR</button>
                        </fieldset>

                        
                    </div>

                </div>
                <?php require "../includes/alerts.php";?>
            </div>
        </div>
    </div>

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
 
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="../data/flot-data.js"></script>
    
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/monitor.js"></script>
    <script src="../dist/js/monitor_adm.js"></script>
    <script src="../dist/js/notificacao.js"></script>




</body>

</html>
