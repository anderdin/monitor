<?php
include_once '../model/Usuarios.php';

if(isset($_POST['operacao'])){
    $operacao = $_POST['operacao'];
}else{
    $operacao = $_GET['operacao'];
}

switch ($operacao){
    case "logon":
        $usuario = new Usuarios();
        $usuario->usuarios_login = $_POST['login'];
        $usuario->usuarios_senha = $_POST['senha'];
        
        $res = $usuario->buscarUsuario($usuario);

        if(count($res) > 0){
            session_start();
            $_SESSION['login'] = $res[0]['usuarios_login'];
            $_SESSION['senha'] = $res[0]['usuarios_senha'];
            $_SESSION['id_usuario'] = $res[0]['idusuarios'];
            $_SESSION['UsuarioNivel'] = $res[0]['usuarios_permissao_nivel'];
            $_SESSION['idUsuario'] = $res[0]['idusuarios'];
        }

        echo json_encode($res);
        break;
        
    case "logoff":
        session_start();
        session_destroy();
        
        header('Location: ../views/index.php');
        break;

    case "novaSenha":
        $loginConfi = new Usuarios();
        $loginConfi->usuarios_senha = md5($_POST['senha']);
        $loginConfi->idusuarios = $_POST['idUsuario'];

        $resLogin = $loginConfi->atualizarUsuario($loginConfi);
        
        echo json_encode($resLogin);
        break;
}
?>
