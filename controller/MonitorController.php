<?php

include_once '../model/Grupos.php';
include_once '../model/Monitor.php';

$operacao = $_POST['operacao'];

switch ($operacao){
    ////////////////////////////////////////////////////////////////////////////////////////////////
    case "carregaMenus":
        $grupos = new Grupos();
        
        $res = $grupos->buscarCtlGrupoUsuariosIdUsuario($_POST['idUsuario']);
        
        
        echo json_encode($res);
        break;
    ////////////////////////////////////////////////////////////////////////////////////////////////    
    case "carregaMonitores":
        $usuario = $_POST['usuario'];
        //////////////////////////////////////////////////////////////
        //Instacias
        $grupoModel = new Grupos();
        $monitorModel = new Monitor();
        //////////////////////////////////////////////////////////////
        //Obter dados
        $grupos = $grupoModel->buscarCtlGrupoUsuariosIdUsuario($usuario);
        $monitores =  $monitorModel->buscarMonitoresUsuario($usuario);
        //////////////////////////////////////////////////////////////
        //Montar array
        
        $t = count($grupos);
        $tt = count($monitores);
        for($i = 0; $t>$i; $i++){
            $grupos[$i]['monitores'] = array();
            for($j=0; $tt>$j; $j++){
                if($grupos[$i]['idgrupos'] == $monitores[$j]['monitores_grupo']){
                    array_push($grupos[$i]['monitores'], $monitores[$j]);
                }
            }
        }
        //////////////////////////////////////////////////////////////
        
        echo json_encode($grupos);
        
        break;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
}