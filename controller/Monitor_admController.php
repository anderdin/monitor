<?php

include_once '../model/Grupos.php';
include_once '../model/Monitor.php';
include_once '../model/Usuarios.php';

$operacao = $_POST['operacao'];

switch ($operacao){
    case "carregaUsuarios":
        $usuariosModel = new Usuarios();
        
        $usuarios = $usuariosModel->buscarTodos();
        
        echo json_encode($usuarios);
        break;

    case "carregaGrupos":
        $grupoModel = new Grupos();
        
        $grupos = $grupoModel->buscarCtlGrupoUsuariosIdUsuario($_POST['idusuario']);
        
        echo json_encode($grupos);
        break;
    
    case "cadastrarGrupo":
        $grupoModel = new Grupos();
        $grupoModel->grupos_nome = $_POST['grupo'];

        $grupos = $grupoModel->cadastrarGrupo($grupoModel);

        echo json_encode($grupos);
        break;

    case "carregaMonitores":
        $monitorModel = new Monitor();
        
        $monitores = $monitorModel->buscarMonitoresIdGrupo($_POST['idGrupo']);
        
        echo json_encode($monitores);
        break;

    case "associacaoMonitor":
        $monitorModel = new Monitor();
        $monitorModel->idmonitores = $_POST['tecnologia'];
        $monitorModel->grupos = $_POST['grupos'];

        $monitores = $monitorModel->associacaoMonitor($monitorModel);

        echo json_encode($monitores);
        break;

    case "cadastrarUsuario":

        $usuariosModel = new Usuarios();

        $usuariosModel->usuarios_login = $_POST['login'];
        $usuariosModel->usuarios_email = $_POST['email'];
        $usuariosModel->permissao = $_POST['permissao'];
        $usuariosModel->grupos = $_POST['grupos'];

        $res = $usuariosModel->cadastrarUsuarios($usuariosModel);
       
       //var_dump($res);
       
        echo json_encode($res);
        break;

    case "deletaUsuario":

        $usuariosModel = new Usuarios();
        $usuariosModel->deletarUsuario($_POST['idUsuario']);
        echo json_encode("1");
        break;
}